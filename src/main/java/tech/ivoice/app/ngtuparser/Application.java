package tech.ivoice.app.ngtuparser;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;

import static java.lang.String.format;
import static java.lang.System.out;

public class Application {
    private static final CSVFormat CSV_FORMAT = CSVFormat.DEFAULT.withSkipHeaderRecord();

    public static void main(String[] args) throws IOException {
        int pages = Integer.valueOf(args[0]);

        PageParser pageParser = new PageParser();

        Path path = Paths.get("qa.csv");
        try (BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW)) {
            CSVPrinter csvFilePrinter = new CSVPrinter(writer, CSV_FORMAT);
            appendCsv(csvFilePrinter, "Question", "Answer");

            for (int page = 1; page <= pages; page++) {
                out.println(format("processing page %d", page));
                Map<String, String> pageQa = pageParser.parsePage(page);
                pageQa.forEach((key, value) -> appendCsv(csvFilePrinter, key, value));
                writer.flush();
            }
        }
    }

    private static void appendCsv(CSVPrinter csvFilePrinter, String question, String answer) {
        try {
            csvFilePrinter.printRecord(question, answer);
        } catch (IOException e1) {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
    }
}
